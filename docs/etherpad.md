# Etherpad

Etherpad Lite es una aplicación externa al panel de control que permite la edición colaborativa de documentos online (Pads) en tiempo real y entre múltiples personas.

Además, los servidores creados con MaadiX incluyen por defecto, junto con esta aplicación, una extensión que permite habilitar áreas de trabajo privadas. De esta manera, se puede elegir si los documentos creados son accesibles al público en general o solamente a l-s usuari-s que tengan una cuenta activada.

# Etherpad admin

La aplicación Etherpad Lite incluye un panel propio de administración que permite:

* Editar preferencias.
* Instalar o desinstalar plugins.
* Reiniciar la aplicación.

El área de administración de esta aplicación está disponible en la dirección:
https://*minombreenmaadix*.maadix.org/etherpad/admin/

En caso de que tuvieras un dominio propio activado en el servidor:
https://*midominio*.com/etherpad/admin/

Para poder acceder a esta área, tendrás que insertar la cuenta admin y la contraseña de la aplicación (estos datos están incluidos en el correo que recibes automáticamente, una vez activada la herramienta).

![Login in the administration area](img/etherpad/login_admin.png)

Solamente el usuario admin puede acceder a esta área, en la que tiene acceso a todos los grupos y usuari-s creados en el sistema. Su contraseña no es válida para operar desde el front-end.

## Cambiar contraseña de admin

Una vez dentro del área de administrador, es recomendable que cambies la contraseña. Para hacerlo, haz clic en la pestaña 'Settings' del menú de la izquierda.
Cuando lo hagas, se abrirá un archivo en texto plano que contiene todos los parámetros de tu instalación.
Haz scroll hasta el final de este archivo, donde encontrarás unas líneas parecidas a las siguientes:

      "users": {
        "admin": {
        "password": "yourpasswordhere",
        "is_admin": true
        }
      }

1. Cambia el valor del password por tu nueva contraseña, teniendo cuidado de ponerlo entre las comillas.
2. Haz clic en 'Save Settings'.
3. Haz clic en 'Restart Etherpad'.

![Change etherpad password](img/ch-paswd.png)


# Área Privada de Etherpad

MaadiX ha desarrollado un plugin de Etherpad Lite, [ep_maadix](https://github.com/MaadixNet/ep_maadix), que permite crear espacios privados de trabajo, y que se instala por defecto junto con la aplicación. Desde el área de administración de Etherpad Lite (*midominio.com*/etherpad/admin/) se pueden establecer preferencias de configuración para este plugin. Haciendo clic en la pestaña 'Users and groups' de la columna izquierda aparecerán las siguientes opciones:

* **Allow users to recover lost password**: activar esta opción permitirá a l-s usuari-s recuperar su contraseña. De forma general, es aconsejable dejarla activada, ya que esto permite a l-s usuari-s restablecer por sí mismos su contraseña si la pierden, evitando tener que enviarla por correo u otro canal.

* **Allow users to register**: si esta opción está activada, cualquier persona podrá crearse una cuenta sin necesidad de recibir una invitación. De lo contrario, sólo l-s usuari-s con una invitación válida podrán acceder a la aplicación. Si un usuario se registra, no podrá acceder a los grupos ya creados hasta que no reciba una invitación. Solamente podrá crear nuevos grupos, nuevos pads e invitar usuari-s a su grupo.

* **Allow public pads**: permite crear pads públicos sin necesidad de tener un usuario activado ni pertenecer a ningún grupo. Los grupos y pads privados siguen estando disponibles aunque esta opción esté activada. Si se quiere evitar que cualquier persona pueda crear nuevos pads, se puede desactivar. Para que se apliquen los cambios hay que reiniciar el servicio, en la pestaña 'Settings' > clic 'Restart Etherpad'.

![Users and groups settings](img/etherpad/settings_plugin.png)

## Crear usuari-s

Desde el área de administración (*midominio.com*/etherpad/admin/) puedes crear grupos e invitar usuari-s. Si has elegido no permitir que l-s usuari-s se registren sin invitación, tendrás que crear al menos una cuenta. Est- usuari- así creado podrá empezar a administrar grupos desde el front-end de la aplicación. Recuerda que las credenciales del usuari- con permisos de administración no son válidas para operar desde el front-end (*midominio.com*/etherpad/).

Para crear grupos e invitar usuari-s desde el área de administrador haz clic en las pestañas 'Manage Groups' o 'Manage User', que encuentras en la cabecera de la sección 'Users and groups' de la columna izquierda (página principal del plugin).

![Create users](img/etherpad/users_groups.png)

## Etherpad front-end

L-s usuari-s que tengan una cuenta activada pueden administrar grupos, invitar otr-s usuari-s, crear y editar documentos (Pads) desde el front-end.

Pueden acceder al front-end de la aplicación en la dirección:
https://*minombreenmaadix*.maadix.org/etherpad/

o si tienes un dominio propio activado en el servidor:
https://*midominio*.com/etherpad/

Para acceder al área privada, hay que identificarse haciendo clic en 'Login', en la parte superior derecha de la página.

![Access front-end](img/etherpad/login_front-end.png)

### Grupos

Los Pads privados y l-s usuari-s deben estar asociados a un grupo.
Un mismo usuari- puede pertenecer a uno o más grupos, además de poder crear uno propio.
Si un- usuari- no pertenece a ningún grupo, tendrá que crear uno antes de poder crear documentos o invitar nuev-s usuari-s.

Se pueden consultar los grupos a los que se tiene acceso o crear nuevos haciendo clic en 'My groups', una vez dentro de la aplicación (Login).

#### ¿Cómo crear un grupo privado?

En "Create a new private group" (Crear un nuevo grupo privado) inserta un nombre de grupo (este nombre no debe existir aún en el sistema) y haz clic en el botón "Create" (Crear). El nuevo grupo aparecerá en la misma página. Una vez hecho esto, podrás empezar a invitar otr-s usuari-s o crear nuevos documentos.

### Invitar usuari-s

Para añadir usuari-s a un grupo, haz clic en "View / Add Users" (Ver / Agregar usuari-s) desde la tabla de la página "My groups". En el campo "Invite user to this group" (Invitar usuario a este grupo) inserta una dirección de correo electrónico válida del usuario que quieras crear. Si la dirección insertada no está registrada todavía en el sistema, se le enviará un correo electrónico de confirmación con las instrucciones para activar la cuenta. También deberás elegir el rol que deseas asignar para est- usuari- en este grupo.

### Roles

El rol asignado a un- usuari- solamente se aplica a un determinado grupo. Un- usuari- puede tener acceso a varios grupos con diferentes roles en cada uno de ellos. Si un- usuari- crea un nuevo grupo, su rol para dicho grupo será siempre 'Admin'.

Un- usuari- nunca puede asignar un rol más alto que su propio rol dentro de un grupo.

Los roles disponibles son:

   **Group Author**: puede crear y editar Pads

   **Group Manager**: puede crear / editar / borrar Pads e invitar / quitar usuari-s

   **Group Admin**: puede crear / editar / borrar Pads, invitar / eliminar usuari-s y borrar todo el grupo

Est- nuev- usuari- invitado aparecerá en la tabla de abajo. El rol asignado a est- usuari- se puede modificar y editar más tarde.

### ¿Cómo crear un Pad privado?

Los Pads se deben crear desde la página de la lista de Pads de un grupo específico, ya que cada Pad sólo puede pertenecer a un grupo determinado. Para crear un nuevo Pad, haz clic en "View / Add Pads" (Ver / Agregar Pads) en la tabla de la página "My groups". En la nueva página que se abre, inserta el nombre del Pad que quieres crear en el campo "Add a Private Pad to this Group" (Crear Pad privado para este grupo) y haz clic en el botón "Create" (Crear). El nuevo pad aparecerá en la tabla de abajo.

El nombre del Pad debe ser único para cada grupo.

### Visitar Pads existentes

Para abrir un Pad ya creado, haz clic en "View / Add Pads" (Ver / Agregar Pads) en la tabla de la página "My groups", en la línea correspondiente al grupo en el que el documento fue creado. Encontrarás un listado de todos los Pads ordenados por fecha de última edición del grupo seleccionado.
Puedes invertir este orden para que se muestren las ediciones más antiguas primero, o reordenar el listado por orden alfabético utilizando las flechas de las columnas correspondientes.

Si te quedara alguna duda, puedes encontrar [en este enlace](https://github.com/ether/etherpad-lite/wiki) la documentación oficial de la aplicación Etherpad Lite.
