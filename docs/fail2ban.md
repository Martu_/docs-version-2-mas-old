# Fail2ban

Fail2ban es un software que se usa para prevenir intentos masivos de conexión, es software libre y en MaadiX viene instalado por defecto. Su función básica es evitar intrusiones que puedan venir de ataques de fuerza bruta (ataques que consisten en probar miles de intentos de conexión para tratar de dar con un user/pass válido).

Funciona de la siguiente manera: **cuando hay un cierto número de intentos fallidos de acceso al servidor, fail2ban bloquea la dirección IP** desde la cuál se intenta el acceso.

En Maadix, fail2ban está activado para los siguientes servicios (*jails*):

* apache: el servidor web para las aplicaciones web, nextcloud, discourse, etc
* sshd: el protocolo de comunicación remoto y seguro con nuestro servidor con el que acceder a su línea de comandos.
* ssh-ddos: implementación específica de ssh para protegerlo de ataques de denegación de servicio distribuidos (ddos).
* dovecot: servidor IMAP/POP3 para conectar a las cuentas de correo.
* mxcp:  la interfaz gráfica de MaadiX (El panel de control).
* sasl: framework de seguridad instalado en el servidor para sincronizar y autenticar protocolos de connexión y autenticación.

En fail2ban la protección para cada uno de estos servicios se les llama *jails*.

## Desbloquear una IP

Puede ocurrir que una persona falle reiteradamente al insertar la contraseña tratando de acceder a un servicio y que fail2ban la bloquee durante un tiempo (normalmente 12h, excepto para la interfaz Webail Rainloop que solo será 1h).

Cuando ocurre esto (y no se quiere esperar 12h para recuperar el acceso), se tendrá que entrar por ssh al servidor para desbloquear la dirección IP que ha sido baneada (bloqueada).

Para este proceso se necesita ejecutar comandos para los que se requieren permisos de administración del sistema, por lo que será necesario loguearse como superusuari- (admin):

`ssh admin@nombreservidor.maadix.net`

**Nota**: si es tu propia IP la que está "baneada" no vas a poder acceder, te devolverá un error 'Connection Refused'. Te recomendamos que te conectes a una VPN, a una red distinta, o que uses la conexión de datos de un teléfono móvil, para que así puedas hacer el login desde una IP distinta a la "baneada".

Para desbloquear una IP primero tenemos que averiguar cuál es la IP en cuestión. Hay distintas formas de ver cuál es la IP con la que se está accediendo a Internet, la más sencilla es ir a cualquier buscador para consultar "cual es mi ip", cientos de resultados mostrarán la dirección IP de tu conexión (por ejemplo [https://cualesmiip.com](https://cualesmiip.com)). Una dirección IP tiene esta forma: 145.234.47.104 (cuatro dígitos separados por un .).

Todas las personas de una misma oficina o una misma vivienda, que están conectadas al mismo router van a salir a Internet con la misma IP (IP pública), por ello puede ocurrir que toda una oficina o vivienda se quede sin acceso al servidor (o alguno de sus servicios) debido a que alguna persona haya realizado demasiados intentos de conexión fallidos.

Una vez ya sabemos la IP bloqueada, se puede desbloquearla con el siguiente comando:

`sudo fail2ban-client set <JAIL> unbanip <IP_A_DESBLOQUEAR>`

Las *jails* activadas por defecto en MaadiX son: sshd, ssh-ddos, apache, dovecot, mxcp o sasl u otras que se hayan podido activar manualmente.  

Para comprobar el estado de cada *jail* y ver qué IPs están "baneadas", se puede ejecutar:

`sudo fail2ban-client status <JAIL>`

Para comprobar el estado de fail2ban y qué *jails* están activadas, se puede ejecutar:

`sudo fail2ban-client status`


Los logs de fail2ban se pueden revisar en: `/var/log/fail2ban.log`

Si se quiere investigar más sobre fail2ban, puedes consultar la [página oficial](https://www.fail2ban.org) de Fail2ban.
